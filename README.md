# Next.js Task Tracker

Welcome to the Task Tracker is a task management application built with Next.js. This project is designed to showcase the integration of a modern tech stack including a cloud database, Prisma ORM, TypeScript, Radix UI, Tailwind CSS, NextAuth.js, alongside implementing SEO optimizations and server-side rendering (SSR).

## Features

- **Task Management:** Create, update, delete, and organize your tasks efficiently.
- **Cloud Database Integration:** Utilizes a cloud database for robust and scalable data storage.
- **Prisma ORM:** Leverages Prisma ORM for efficient database management and queries.
- **TypeScript:** Built with TypeScript to ensure type safety and reduce runtime errors.
- **Radix UI Components:** Implements Radix UI for accessible and customizable UI components.
- **Tailwind CSS:** Styled with Tailwind CSS for rapid UI development without sacrificing design.
- **NextAuth.js Authentication:** Secures the application with NextAuth.js for comprehensive authentication solutions.
- **SEO Optimized:** Incorporates SEO best practices to ensure the application is search engine friendly.
- **Server-Side Rendering (SSR):** Utilizes Next.js's SSR capabilities for faster load times and improved SEO.

## Getting Started

### Prerequisites

Before you begin, ensure you have the following installed on your system:
- Node.js (latest version)
- A supported cloud database (Refer to the `.env.example` for environment variable settings)

### Setup

1. **Clone the repository**

2. **Install dependencies**

```bash
yarn
```

3. **Configure your environment**

4. **Run the development server**

```bash
yarn dev
```